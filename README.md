### Design pattern in PHP

- Tìm các design pattern được sử dụng trong package: https://github.com/thephpleague/flysystem/tree/master/src

## [Prototype](https://designpatternsphp.readthedocs.io/en/latest/Creational/Prototype/README.html)

![](./images/fileException.png)
![](./images/fileException2.png)

## [Builder](https://designpatternsphp.readthedocs.io/en/latest/Creational/Builder/README.html)

![](./images/Builder.png)
![](./images/Build2.png)

